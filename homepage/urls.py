from django.urls import path

from .views import index, get_json

urlpatterns = [
    path('', index, name='index'),
    path('get_json/', get_json, name='get_json'),
]