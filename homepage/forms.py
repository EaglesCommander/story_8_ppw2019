from django import forms

class Searchbar(forms.Form):

        query_attrs = {'type':'query','class':'form-control', 'placeholder':'Search for something!', 'id':'query-form'}
        
        query = forms.CharField(label='', required=True, max_length=200, widget=forms.TextInput(attrs=query_attrs))