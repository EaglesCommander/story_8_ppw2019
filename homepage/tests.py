from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from .views import index, get_json
from .forms import Searchbar

class HomepageFunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        options = Options()
        options.headless = True
        self.selenium = webdriver.Firefox(options=options)
        self.selenium.implicitly_wait(3)
        super(HomepageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(HomepageFunctionalTest, self).tearDown()

    def test_page_works_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        self.assertIn('Google Books Search', self.selenium.page_source)
        time.sleep(3)
        self.assertIn('朝焼けは黄金色', self.selenium.page_source)
    
    def test_search_works_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        form = selenium.find_element_by_id('query-form')
        form.send_keys('LoveLive')

        button = selenium.find_element_by_id('submit')
        button.click()

        time.sleep(10)

        self.assertIn('LoveLive! School idol diary (4)', self.selenium.page_source)

class HomepageUnitTest(TestCase):

    def test_homepage_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
        
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_json_found(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_using_json_func(self):
        found = resolve('/get_json/')
        self.assertEqual(found.func, get_json)

    def test_forms_works(self):
        searchbar = Searchbar()

        self.assertIsInstance(searchbar, Searchbar)
