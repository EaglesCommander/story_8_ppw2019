from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
import requests
from .forms import Searchbar

def index(request):

    response = {"searchbar":Searchbar}

    return render(request, 'index.html', response)

def get_json(request):
    query = request.GET.get('query', None)
    url = "https://www.googleapis.com/books/v1/volumes?q=" + query

    return JsonResponse(requests.get(url).json())