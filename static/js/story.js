$(document).ready(function(){
    var query = "THE IDOLM@STER";
    search_query(query);
});

$(document).on("submit", "#search-query", function(event){
    event.preventDefault();
    search_query(null);    
});

function search_query(query){
    console.log("y")
    if (query == null){
        var query = $('#query-form').val();
    }

    $.ajax({
        type:"GET",
        url:"/get_json",
        data:{
            'query': query
        },
        dataType: 'json',
        success: function(json){
            $("#search-result").empty();
            $("#search-result").append('<div class="row"><div class="col-1 text-center"><p class="h2">Image</p></div><div class="col-3 text-center"><p class="h2">Title</p></div><div class="col-2 text-center"><p class="h2">Author</p></div><div class="col-6 text-center"><p class="h2">Description</p></div><br>');
            var books = json.items;

            for(i=0; i < books.length ; i++) {

                var title = books[i].volumeInfo.title;
                if (title == undefined){
                    title = "Undefined title";
                }

                var desc = books[i].volumeInfo.description;
                if (desc == undefined){
                    desc = "No description available";
                }

                var author = books[i].volumeInfo.authors[0];
                if (author == undefined){
                    author = "No author available";
                }

                $("#search-result").append('<div class="row"><div class="col-1 text-center" id="' + i + '"></div><div class="col-3 text-center"><p>' + title +'</p></div><div class="col-2 text-center"><p>' + author +'</p></div><div class="col-6 text-center"><p>' + desc + '</p></div></div><br>');
                
                var id = "#" + i
                try {
                    var link = books[i].volumeInfo.imageLinks.smallThumbnail;
                }
                catch {
                    var link = "empty"
                }

                load_image(id, link);
              }
        }
    });
    
    return false;
}

function load_image(id, link){
    if (link == "empty"){
        $(id).append("No image available");
        return false;
    }

    var img = $("<img />").attr('src', link).on('load', function(){
        if (!img.complete){
            $(id).append(img);
        }
        else {
            $(id).append("Image loading error");
        }
    })

    return false;
}